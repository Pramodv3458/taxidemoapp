package com.example.taxidemo.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.pdf.PdfRenderer;
import android.preference.Preference;

import java.util.function.Predicate;

public class SharedPrefrence {
    private static SharedPrefrence preference;
    private SharedPreferences sharedPreferences;
    SharedPreferences.Editor prefEditor;

    public static SharedPrefrence getInstance(Context context) {
        if (preference == null) {
            preference = new SharedPrefrence(context);
        }
        return preference;
    }

    private SharedPrefrence(Context context) {
        sharedPreferences = context.getSharedPreferences("Taxidemo", Context.MODE_PRIVATE);
    }

    public void savedata(String key, String value) {
        prefEditor = sharedPreferences.edit();
        prefEditor.putString(key, value);
        prefEditor.commit();
    }

    public String getdata(String key) {
        if (sharedPreferences != null) {
            return sharedPreferences.getString(key, "");
        }
        return "";
    }

    public void clear() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}