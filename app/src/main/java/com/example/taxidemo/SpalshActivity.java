package com.example.taxidemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.taxidemo.util.ConstantClass;
import com.example.taxidemo.util.SharedPrefrence;


public class SpalshActivity extends AppCompatActivity {
    ImageView imageView;
    private static int SPLASH_TIME_OUT = 4000;
    SharedPrefrence yourprefrence;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);

        yourprefrence=SharedPrefrence.getInstance(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (yourprefrence.getdata(ConstantClass.EMAIL).length()>5)
                {
                    startActivity(new Intent(SpalshActivity.this,MainActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(SpalshActivity.this,LoginAcitivity.class));
                    finish();
                }

            }
        },SPLASH_TIME_OUT);
    }
}